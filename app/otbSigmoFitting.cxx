/*=========================================================================
  Program:   phenotb
  Language:  C++

  Copyright (c) CESBIO. All rights reserved.

  See phenotb-copyright.txt for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"
#include "otbWrapperChoiceParameter.h"

#include "phenoFunctions.h"
#include "phenoTwoCycleSigmoFittingFunctor.h"

namespace otb
{
namespace Wrapper
{

class SigmoFitting : public Application
{
public:
/** Standard class typedefs. */
  typedef SigmoFitting     Self;
  typedef Application Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;
  
/** Standard macro */
  itkNewMacro(Self);
  itkTypeMacro(SigmoFitting, otb::Application);

  using FunctorType =
    pheno::TwoCycleSigmoFittingFunctor<FloatVectorImageType::PixelType>;
  using FilterType = pheno::BinaryFunctorImageFilterWithNBands<FloatVectorImageType,
                                                               FloatVectorImageType,
                                                               FunctorType>;

private:
  void DoInit() override
  {
    SetName("SigmoFitting");
    SetDescription("Extract phenological information from time profiles");

    // Documentation
    SetDocLongDescription("This module implements several algorithms allowing to extract \
                           phenological information from time profiles. These time profiles \
                           should represent vegetation status as for instance NDVI, LAI, etc");
    SetDocLimitations("None");
    SetDocAuthors("Jordi Inglada");

    AddDocTag("Phenology");

    AddParameter(ParameterType_InputImage,  "in",   "Input Image");
    SetParameterDescription("in", "Input image");
    MandatoryOn("in");

    AddParameter(ParameterType_InputImage,  "mask",   "Mask Image");
    SetParameterDescription("mask", "Input validity mask");
    MandatoryOff("mask");

    AddParameter(ParameterType_String, "dates", "Date File");
    SetParameterDescription("dates", "Name of the file containing the dates for the images");
    MandatoryOn("dates");

    AddParameter(ParameterType_String, "mode", "Processing mode: profile, params or metrics");
    SetParameterDescription("mode", "Choose between producing as output the reprocessed profile, the parametres of the two-cycle sigmoid or the derived phenology metrics. Default value is profile.");
    MandatoryOff("mode");

    AddParameter(ParameterType_OutputImage, "out",  "Output Image");
    SetParameterDescription("out", "Output image");
    MandatoryOn("out");
    
    AddRAMParameter();

    SetDocExampleParameterValue("in", "TimeSeriesNDVI.tif");
    SetDocExampleParameterValue("mask", "TimeSeriesMasks.tif");
    SetDocExampleParameterValue("dates", "GapFilledTimeSeriesOutputDates.tif");
    SetDocExampleParameterValue("mode", "profile");
    SetDocExampleParameterValue("out", "timeSeries_img.tif");

    SetOfficialDocLink();
  }

  virtual ~SigmoFitting() override
  {
  }


  void DoUpdateParameters() override
  {
    // Nothing to do here : all parameters are independent
  }


  void DoExecute() override
  {

    // prepare the vector of dates
    auto dates = GapFilling::parse_date_file(this->GetParameterString("dates"));
    // pipeline
    FloatVectorImageType::Pointer inputImage = this->GetParameterImage("in");
    FloatVectorImageType::Pointer maskImage;
    bool use_mask = true;
    if(IsParameterEnabled("mask"))
      maskImage = this->GetParameterImage("mask");
    else
      {
      maskImage = inputImage;
      use_mask = false;
      otbAppLogINFO("No mask will be used.\n");
      }
    inputImage->UpdateOutputInformation();
    maskImage->UpdateOutputInformation();
    bool fit_mode = true;
    bool metrics_mode = false;
    unsigned int nb_out_bands = inputImage->GetNumberOfComponentsPerPixel();
    if(IsParameterEnabled("mode") && GetParameterString("mode") != "profile")
      {
      nb_out_bands = 12;
      fit_mode = false;
      if(GetParameterString("mode") == "params")
        {
        otbAppLogINFO("Parameter estimation mode.\n");
        }
      if(GetParameterString("mode") == "metrics")
        {
        metrics_mode = true;
        otbAppLogINFO("Metrics estimation mode.\n");
        }
      }

    filter = FilterType::New();

    filter->SetInput(0, inputImage);
    filter->SetInput(1, maskImage);
    filter->GetFunctor().SetDates(dates);
    filter->GetFunctor().SetUseMask(use_mask);
    filter->GetFunctor().SetReturnFit(fit_mode);
    filter->GetFunctor().SetReturnMetrics(metrics_mode);
    filter->SetNumberOfOutputBands(nb_out_bands);
    filter->UpdateOutputInformation();
    SetParameterOutputImage("out", filter->GetOutput());
  }

  FilterType::Pointer filter;

};

}
}

OTB_APPLICATION_EXPORT(otb::Wrapper::SigmoFitting)
