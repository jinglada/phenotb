/*=========================================================================

  Program:   phenotb
  Language:  C++

  Copyright (c) Jordi Inglada. All rights reserved.

  See phenotb-copyright.txt for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#ifndef _PHENO2CFF_H_
#define _PHENO2CFF_H_

#include "phenoFunctions.h"

namespace pheno
{
template <typename PixelType>
class TwoCycleSigmoFittingFunctor
{
public:
  struct DifferentSizes {};
  TwoCycleSigmoFittingFunctor();
  void SetDates(const std::vector<tm>& d);
  void SetUseMask(bool um);
  void SetReturnFit(bool rf);
  void SetReturnMetrics(bool rm);
  void SetFitOnlyInvalid(bool foi);
  PixelType operator()(PixelType pix, PixelType mask);
  bool operator!=(const TwoCycleSigmoFittingFunctor a);
  bool operator==(const TwoCycleSigmoFittingFunctor a);
  
protected:
  PixelType ExtractParameters(const VectorType& x_1, const MinMaxType& mm1, 
                              const VectorType& x_2, 
                              const MinMaxType& mm2) const;

  PixelType ComputeFit(const VectorType& x_1, const MinMaxType& mm1, 
                       const VectorType& x_2, 
                       const MinMaxType& mm2, 
                       std::size_t nbDates, 
                       const PixelType& pix, 
                       const VectorType& mv) const;

  PixelType ComputeMetrics(const TwoCycleApproximationResultType& approx) const;

  std::vector<tm> dates;
  VectorType dv;
  bool return_fit;
  bool return_metrics;
  bool fit_only_invalid;
  bool use_mask;
  const std::size_t m_MinNbDates = 4;

};
}//namespace pheno
#include "phenoTwoCycleSigmoFittingFunctor.txx"
#endif
